package com.ah.bsib_be;

import com.ah.bsib_be.config.UploadDirectory;
import com.ah.bsib_be.controller.UploadController;
import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;


@EnableJpaAuditing
@SpringBootApplication
//@SpringBootApplication(exclude = {
//    JpaRepositoriesAutoConfiguration.class
//})
//@EnableConfigurationProperties({
//    UploadController.class
//})
@EnableEncryptableProperties
public class BsibBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(BsibBeApplication.class, args);
    }
}
