package com.ah.bsib_be.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
//@ConfigurationProperties(prefix = "file.upload")
@ConfigurationProperties(prefix = "bsib-list")
public class UploadDirectory {
    private String location;
    private List<String> extensions = new ArrayList<String>();

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<String> getExtensions() {
        return extensions;
    }
}
