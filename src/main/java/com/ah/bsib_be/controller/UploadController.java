package com.ah.bsib_be.controller;

import com.ah.bsib_be.config.UploadDirectory;
import com.ah.bsib_be.entity.ModuleEntity;
import com.ah.bsib_be.entity.User;
import com.ah.bsib_be.service.*;
import io.swagger.annotations.Api;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.persistence.EntityNotFoundException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@RestController
@Api(tags = "Upload")
@RequestMapping("/v1")
public class UploadController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    //private final StorageService storageService; // 'final' need constructor
    private StorageService storageService;

    @Autowired
    private MailService mailService;

    @Autowired
    private UserService userService;

    @Autowired
    private ModuleService moduleService;

    @Autowired
    private DocumentService documentService;

    @Autowired
    private InfoService infoService;

    @Autowired
    private UploadDirectory config = new UploadDirectory();

    public UploadController(StorageService storageService) {
        this.storageService = storageService;
    }

//    @GetMapping
//    public String getMessages() throws MailException, MessagingException {
//        user.setEmail("ajmerhasan@gmail.com");
//
//        //mailService.sendEmail(user); // without attachment
//        //mailService.sendEmailWithAttachment(user); // with attachment
//
//        return "Congratulations! Your mail has been send to the user.";
//    }

    //@GetMapping("/download/{filename:.+}")
    @GetMapping("/download/{filename}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String filename) throws MalformedURLException, FileNotFoundException {

        Resource resource = storageService.loadFile(filename);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    // upload single file
    @PostMapping("/upload")
    //public ResponseEntity<FileResponse> uploadSingleFile (//@RequestParam("file") MultipartFile file,
    //public ResponseEntity<Object> uploadSingleFile (@RequestParam(value = "file", required = false) Optional<MultipartFile> file ,
    public ResponseEntity<Object> uploadSingleFile (@RequestParam(value = "file", required = false) MultipartFile file,
                                                    @RequestParam("data") String payload) throws IOException {
        // for multi attachment
        //List<String> fileList = new ArrayList<>();
        //List<String> fileUrl = new ArrayList<>();

        /** 1. register into table user **/
        User usr = new User();
        User savedUsr = new User();
        usr.setEmail("ajmerhasan@gmail.com");
        savedUsr = userService.save(usr);

        /** 2. register into table module **/
        // convert string into json object
        JSONObject jayload = new JSONObject(payload);
        ModuleEntity moduleEntity = new ModuleEntity();
        moduleEntity = moduleService.save(jayload, savedUsr);

        /** 3. register into table info **/
        infoService.save(jayload, moduleEntity.getId(), savedUsr);

        /** 4. upload & register into table document **/
        // only accept images and pdf
        //if (file.isPresent()) {

        // not working with flutter - received as octet
        /**
        if (!Arrays.asList(
                ContentType.IMAGE_JPEG.getMimeType(),
                ContentType.IMAGE_PNG.getMimeType(),
                //ContentType.IMAGE_GIF.getMimeType(),
                "application/pdf"
        ).contains(file.getContentType()) ) {
            System.out.println("\n" + file.getContentType() + "\n");
            throw new IllegalStateException();
        }
        **/

        List<String> list = new ArrayList<String>();

        String fileTxt = file.getOriginalFilename();

        int index = fileTxt.lastIndexOf('.');

        if (config.getExtensions().contains(fileTxt.substring(index + 1))) {
            logger.info(fileTxt + " contains " + fileTxt.substring(index + 1));
        } else {
            logger.warn(fileTxt + " has invalid file type " + fileTxt.substring(index + 1));
            throw new FileUploadException();
        }

        // upload file
        String upfile = storageService.saveFile(file);

        // for multi attachment // todo
        //fileList.add(upfile);
        //fileUrl.add(fileDownloadUri);

        // generate download link
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/v1/download/")
                .path(upfile)
                .toUriString();

        documentService.save(moduleEntity.getId(), savedUsr, upfile, fileDownloadUri);
        //mailService.sendEmail(savedUsr, fileList, fileUrl); // todo multi attachment

        mailService.sendEmail(savedUsr, upfile, fileDownloadUri); // single attachment
        //}

        //return ResponseEntity.status(HttpStatus.OK).body(new FileResponse(upfile, fileDownloadUri,"File uploaded successfully!"));
        return ResponseEntity.ok().build();
    }

    /**
    // upload multiple files
    @PostMapping("/uploads")
    public ResponseEntity<List<FileResponse>> uploadMultipleFiles (@RequestParam("files") MultipartFile[] files) {
        user.setEmail("ajmerhasan@gmail.com");

        List<String> fileList = new ArrayList<>();
        List<String> fileUrl = new ArrayList<>();

        List<FileResponse> responses = Arrays
            .asList(files)
            .stream()
            .map(
                file -> {
                    String upfile = null;
                    try {
                        if (!Arrays.asList(
                                ContentType.IMAGE_JPEG.getMimeType(),
                                ContentType.IMAGE_PNG.getMimeType(),
                                //ContentType.IMAGE_GIF.getMimeType(),
                                "application/pdf"
                        ).contains(file.getContentType()) ) {
                            throw new IllegalStateException();
                        }

                        upfile = storageService.saveFile(file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                        .path("/v1/download/")
                        .path(upfile)
                        .toUriString();

                    fileList.add(upfile);
                    fileUrl.add(fileDownloadUri);
                    return new FileResponse(upfile,fileDownloadUri,"File uploaded with success!");
                }
            )
            .collect(Collectors.toList());

        mailService.sendEmail(user, fileList, fileUrl); // without attachment
        return ResponseEntity.status(HttpStatus.OK).body(responses);
    }
    **/
}
