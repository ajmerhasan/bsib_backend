package com.ah.bsib_be.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(
        value = {"createdAt", "createdby", "updatedAt", "updatedBy"},
        allowGetters = true,
        allowSetters = true
)
public class Audit implements Serializable {
    @ApiModelProperty(required = false, hidden = true)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createddate", updatable = false)
    @CreatedDate
    private Date createdAt;

    @ApiModelProperty(required = false, hidden = true)
    @Column(name = "createdby", updatable = false)
    @CreatedBy
    private String createdBy;

    @ApiModelProperty(required = false, hidden = true)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updatedAt", updatable = false)
    @CreatedDate
    private Date updatedAt;

    @ApiModelProperty(required = false, hidden = true)
    @Column(name = "lastupdateby", updatable = true)
    @LastModifiedBy
    private String updatedBy;

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
}
