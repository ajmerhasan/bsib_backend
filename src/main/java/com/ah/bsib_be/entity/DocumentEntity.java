package com.ah.bsib_be.entity;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "bsib_document")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class DocumentEntity extends Audit {
    @ApiModelProperty(required = false, hidden = true)
    @Id
    @Column(name = "bsib_document_guid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "bsib_module_guid", nullable = false)
    //@JsonManagedReference
    public ModuleEntity moduleId;

    @ApiModelProperty(required = false, hidden = true)
    //@NotNull
    @Column(name = "document_name")
    private String documentName;

    @ApiModelProperty(required = false, hidden = true)
    //@NotNull
    @Column(name = "location_path")
    private String url;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public ModuleEntity getModuleId() {
        return moduleId;
    }

    public DocumentEntity setModuleId(ModuleEntity moduleId) {
        this.moduleId = moduleId;
        return this;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
