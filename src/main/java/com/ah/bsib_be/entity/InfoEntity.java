package com.ah.bsib_be.entity;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "bsib_info")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class InfoEntity extends Audit {
    @ApiModelProperty(required = false, hidden = true)
    @Id
    @Column(name = "bsib_info_guid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "bsib_module_guid", nullable = false)
    //@JsonManagedReference
    public ModuleEntity moduleId;

    @ApiModelProperty(required = false, hidden = true)
    //@NotNull
    @Column(name = "info")
    private String infoName;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public ModuleEntity getModuleId() {
        return moduleId;
    }

    public InfoEntity setModuleId(ModuleEntity moduleId) {
        this.moduleId = moduleId;
        return this;
    }

    public String getInfoName() {
        return infoName;
    }

    public void setInfoName(String infoName) {
        this.infoName = infoName;
    }
}
