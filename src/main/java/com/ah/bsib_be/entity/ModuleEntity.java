package com.ah.bsib_be.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table(name = "bsib_module")
public class ModuleEntity extends Audit {
    @ApiModelProperty(required = false, hidden = true)
    @Id
    @Column(name = "bsib_module_guid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @NotNull
    @Column(name = "module")
    public int module;

    @NotNull
    @Column(name = "major_type")
    public int major_type;

    @NotNull
    @Column(name = "main_type")
    public int main_type;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "bsib_user_guid", nullable = false)
    //@JsonManagedReference
    public User uploader;

    @ApiModelProperty(required = false, hidden = true)
    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "moduleId")
    @JsonBackReference
    private List<InfoEntity> info;

    @ApiModelProperty(required = false, hidden = true)
    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "moduleId")
    @JsonBackReference
    private List<DocumentEntity> document;

    public ModuleEntity() {
    }

    public ModuleEntity(Long id, int module, int major_type, int main_type, User uploader, List<InfoEntity> info, List<DocumentEntity> document) {
        Id = id;
        this.module = module;
        this.major_type = major_type;
        this.main_type = main_type;
        this.uploader = uploader;
        this.info = info;
        this.document = document;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public int getModule() {
        return module;
    }

    public void setModule(int module) {
        this.module = module;
    }

    public int getMajor_type() {
        return major_type;
    }

    public void setMajor_type(int major_type) {
        this.major_type = major_type;
    }

    public int getMain_type() {
        return main_type;
    }

    public void setMain_type(int main_type) {
        this.main_type = main_type;
    }

    public User getUploader() {
        return uploader;
    }

    public ModuleEntity setUploader(User uploader) {
        this.uploader = uploader;
        return this;
    }

    public List<InfoEntity> getInfo() {
        return info;
    }

    public void setInfo(List<InfoEntity> info) {
        this.info = info;
    }

    public List<DocumentEntity> getDocument() {
        return document;
    }

    public void setDocument(List<DocumentEntity> document) {
        this.document = document;
    }
}
