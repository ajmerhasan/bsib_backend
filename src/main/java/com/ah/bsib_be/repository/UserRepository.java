package com.ah.bsib_be.repository;

import com.ah.bsib_be.entity.ModuleEntity;
import com.ah.bsib_be.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
}
