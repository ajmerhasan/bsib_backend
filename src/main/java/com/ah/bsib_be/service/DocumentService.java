package com.ah.bsib_be.service;

import com.ah.bsib_be.entity.DocumentEntity;
import com.ah.bsib_be.entity.ModuleEntity;
import com.ah.bsib_be.entity.User;
import com.ah.bsib_be.repository.DocumentRepository;
import com.ah.bsib_be.repository.ModuleRepository;
import com.ah.bsib_be.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;

@Service
public class DocumentService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    UserRepository userRepository;

    @Autowired
    ModuleRepository moduleRepository;

    @Autowired
    DocumentRepository documentRepository;

    public void save(Long moduleId, User savedUsr, String upfile, String fileDownloadUri) {
        DocumentEntity de = new DocumentEntity();
        de.setDocumentName(upfile);
        de.setUrl(fileDownloadUri);
        de.setCreatedBy(savedUsr.getEmail());
        moduleRepository.findById(moduleId).map((Function<ModuleEntity, Object>) de::setModuleId);

        documentRepository.save(de);

        ResponseEntity.ok().build();
    }
}
