package com.ah.bsib_be.service;

import com.ah.bsib_be.entity.InfoEntity;
import com.ah.bsib_be.entity.ModuleEntity;
import com.ah.bsib_be.entity.User;
import com.ah.bsib_be.repository.DocumentRepository;
import com.ah.bsib_be.repository.InfoRepository;
import com.ah.bsib_be.repository.ModuleRepository;
import com.ah.bsib_be.repository.UserRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.function.Function;

@Service
public class InfoService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    UserRepository userRepository;

    @Autowired
    ModuleRepository moduleRepository;

    @Autowired
    InfoRepository infoRepository;

    public void save(JSONObject jayload, Long id, User savedUsr) {

        //todo: change to looping method and json formatting
        JSONArray ja = jayload.getJSONArray("text_box");
        ja.getJSONObject(0).keySet().forEach(k -> {
            Object v = ja.getJSONObject(0).get(k);

            InfoEntity ie = new InfoEntity();
            ie.setInfoName(v.toString());
            ie.setCreatedBy(savedUsr.getEmail());

            moduleRepository.findById(id).map((Function<ModuleEntity, Object>) ie::setModuleId);
            infoRepository.save(ie);

            //System.out.println(">>>>>>> "+"key: "+ k + " value: " + v);
        });

        ResponseEntity.ok().build();
    }
}
