package com.ah.bsib_be.service;

import com.ah.bsib_be.entity.User;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;

@Service
public class MailService {
    private JavaMailSender javaMailSender;



    public MailService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    /**
    * This function is used to send mail without attachment.
    * @param user
    * @throws MailException
    */
    public void sendEmail(User user, String upfile, String fileDownloadUri) throws MailException {

        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(user.getEmail());
        mail.setSubject("Upload file(s) notification - " + upfile);
        mail.setText("You have successfully uploaded following file(s):\n" + fileDownloadUri);

        javaMailSender.send(mail);
    }

    public void sendEmail2(User user, List<String> upfile, List<String> fileDownloadUri) throws MailException {

        StringBuilder str1 = new StringBuilder("");
        StringBuilder str2 = new StringBuilder("");

        for (String filename : upfile) {
            str1.append(filename).append(",");
        }

        for (String fileurl : fileDownloadUri) {
            str2.append("- ").append(fileurl).append("\n");
        }

        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(user.getEmail());
        mail.setSubject("Upload file(s) notification - " + str1);
        mail.setText("You have successfully uploaded following file(s):\n" + str2);

        javaMailSender.send(mail);
    }

    /**
    * This function is used to send mail that contains an attachment.
    *
    * @param user
    * @throws MailException
    * @throws MessagingException
    */
    public void sendEmailWithAttachment(User user) throws MailException, MessagingException {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

        helper.setTo(user.getEmail());
        helper.setSubject("Testing Mail API with Attachment");
        helper.setText("Please find the attached document below.");

        ClassPathResource classPathResource = new ClassPathResource("Attachment.pdf");
        helper.addAttachment(classPathResource.getFilename(), classPathResource);

        javaMailSender.send(mimeMessage);
    }
}
