package com.ah.bsib_be.service;

import com.ah.bsib_be.entity.ModuleEntity;
import com.ah.bsib_be.entity.User;
import com.ah.bsib_be.repository.DocumentRepository;
import com.ah.bsib_be.repository.InfoRepository;
import com.ah.bsib_be.repository.ModuleRepository;
import com.ah.bsib_be.repository.UserRepository;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.Optional;
import java.util.function.Function;

@Service
public class ModuleService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    UserRepository userRepository;

    @Autowired
    ModuleRepository moduleRepository;

    @Autowired
    DocumentRepository documentRepository;

    @Autowired
    InfoRepository infoRepository;

    public ModuleEntity save(JSONObject jayload, User savedUsr) {

        ModuleEntity me = new ModuleEntity();
        me.setModule((Integer) jayload.get("module_type"));
        me.setMajor_type((Integer) jayload.get("major_type"));
        me.setMain_type((Integer) jayload.get("main_type"));

        Long userId = savedUsr.getId();
        userRepository.findById(userId).map((Function<User, Object>) me::setUploader);
        me.setCreatedBy(savedUsr.getEmail());

        return moduleRepository.save(me);
        //ModuleEntity saveModule = moduleRepository.save(me);

//        Optional<ModuleEntity> findModuleId = moduleRepository.findById((long) me.getId());
//        if (findModuleId.isPresent()) {
//
//        } else {
//            throw new EntityNotFoundException("Module " + findModuleId.toString() + " not id database");
//        }

        //return moduleRepository.save(module);
        //return null;
    }
}
