package com.ah.bsib_be.service;

import com.ah.bsib_be.config.UploadDirectory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class StorageService {
    @Autowired
    private UploadDirectory config;
    //String ticketImpact = config.getImpact().get(2); // default low

    private final Path uploadDir;

    public StorageService(UploadDirectory uploadDirectory) throws FileNotFoundException {
        this.uploadDir = Paths.get(uploadDirectory.getLocation()).toAbsolutePath().normalize();

        /**
        try {
            Files.createDirectories(this.uploadDir);
        } catch (Exception ex) {
            throw new FileNotFoundException();
        }
        **/
    }

    @PostConstruct
    public void init() throws IOException {
        Files.createDirectories(this.uploadDir);
    }

    public String saveFile(MultipartFile file) throws IOException {
        String fileName = file.getOriginalFilename();
        Path dfile = this.uploadDir.resolve(fileName);
        Files.copy(file.getInputStream(), dfile,StandardCopyOption.REPLACE_EXISTING);

        return fileName;
    }

    public Resource loadFile(String fileName) throws FileNotFoundException, MalformedURLException {
        Path file = this.uploadDir.resolve(fileName).normalize();
        Resource resource = new UrlResource(file.toUri());

        if (resource.exists() || resource.isReadable()) {
            return resource;
        }
        else {
            throw new FileNotFoundException("Could not find file");
        }
    }
}
