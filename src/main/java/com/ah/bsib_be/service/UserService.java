package com.ah.bsib_be.service;

import com.ah.bsib_be.entity.ModuleEntity;
import com.ah.bsib_be.entity.User;
import com.ah.bsib_be.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import java.util.Optional;

@Service
public class UserService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    UserRepository userRepository;

    public User save(User mail) {
        User userRecord = new User();

        Optional<User> findEmail = Optional.ofNullable(userRepository.findByEmail(mail.getEmail()));

        if (findEmail.isPresent()) {
            logger.info("{}", "user email "+ mail.getEmail() + " is already exists");
            return findEmail.get();
        } else {
            logger.info("{}", "user email "+ mail.getEmail() + " not found, creating new one...");
            return userRepository.save(mail);
        }
    }
}
