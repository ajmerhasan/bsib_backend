package com.ah.bsib_be.swagger;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Profile({"loc","dev"})
@Controller
public class SwaggerController {
    @RequestMapping(value = "/")
    public String swaggerUi() {
        return "redirect:swagger-ui.html";
    }
}
