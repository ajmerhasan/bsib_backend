CREATE TABLE bsib_module(
    bsib_module_guid INT NOT NULL AUTO_INCREMENT,
    module INT(1) NOT NULL,
    major_type INT(1) NOT NULL,
    main_type INT(1) NOT NULL,
    PRIMARY KEY (bsib_module_guid)
);

CREATE TABLE bsib_info(
    bsib_info_guid INT NOT NULL AUTO_INCREMENT,
    bsib_module_guid INT NOT NULL,
    info VARCHAR(30) NOT NULL,
    PRIMARY KEY (bsib_info_guid),
    FOREIGN KEY (bsib_module_guid) REFERENCES bsib_module(bsib_module_guid) ON DELETE CASCADE
);

CREATE TABLE bsib_document(
    bsib_document_guid INT NOT NULL AUTO_INCREMENT,
    bsib_module_guid INT NOT NULL,
    attachment_name VARCHAR(30) NOT NULL,
    PRIMARY KEY (bsib_document_guid),
    FOREIGN KEY (bsib_module_guid) REFERENCES bsib_module(bsib_module_guid) ON DELETE CASCADE
);