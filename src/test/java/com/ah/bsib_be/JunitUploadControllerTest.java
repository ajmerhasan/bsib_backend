package com.ah.bsib_be;

import com.ah.bsib_be.controller.UploadController;
import com.ah.bsib_be.entity.User;
import com.ah.bsib_be.repository.UserRepository;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class JunitUploadControllerTest {
    @Autowired
    private UserRepository userRepository;

    @Test
    public void testCreateUser() throws Exception {
        User testUser = new User();
        testUser.setEmail("test@gmail.com");
        userRepository.save(testUser);

        User targetUser = userRepository.findByEmail("test@gmail.com");
        //System.out.println("----" + testUser.getEmail() + "----");
        assertNotNull(testUser);
        assertEquals(testUser.getEmail(), targetUser.getEmail());
    }
}
