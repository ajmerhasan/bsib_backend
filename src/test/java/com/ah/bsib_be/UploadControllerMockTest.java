package com.ah.bsib_be;

import com.ah.bsib_be.config.UploadDirectory;
import com.ah.bsib_be.controller.UploadController;
import com.ah.bsib_be.entity.ModuleEntity;
import com.ah.bsib_be.entity.User;
import com.ah.bsib_be.repository.DocumentRepository;
import com.ah.bsib_be.repository.InfoRepository;
import com.ah.bsib_be.repository.ModuleRepository;
import com.ah.bsib_be.repository.UserRepository;
import com.ah.bsib_be.service.StorageService;
import com.ah.bsib_be.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@WebMvcTest(UploadController.class)
@ActiveProfiles("test")
public class UploadControllerMockTest {
    @Autowired
    MockMvc mock;

    @MockBean
    UploadController uploadController;

    @MockBean
    StorageService storageService;

    @Test
    public void testUpload() throws Exception {
        String uri = "/upload";

        MockMultipartFile filePdf = new MockMultipartFile(
                "file",
                "contract.pdf",
                MediaType.APPLICATION_PDF_VALUE,
                //null,
                "<<pdf data>>".getBytes(StandardCharsets.UTF_8));

        System.out.println("\n" + filePdf.getOriginalFilename());

        JSONObject json = new JSONObject();
        json.put("module_type", 1);
        json.put("major_type", 1);
        json.put("main_type", 2);
        json.put("text_box1", "test");
        String data = json.toString();

        System.out.println(data + "\n");

        MockMultipartFile jsonData = new MockMultipartFile(
                "data",
                "data",
                MediaType.APPLICATION_JSON_VALUE,
                data.getBytes(StandardCharsets.UTF_8));

        //MockMultipartFile firstFile = new MockMultipartFile("data", "filename.txt", "text/plain", "some xml".getBytes());
        //MockMultipartFile secondFile = new MockMultipartFile("data", "other-file-name.data", "text/plain", "some other type".getBytes());
        //MockMultipartFile jsonFile = new MockMultipartFile("json", "", "application/json", "{\"json\": \"someValue\"}".getBytes());

        //MockMvc mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
//        MvcResult mvcResult = mock
//                .perform(MockMvcRequestBuilders
//                        .multipart("/v1/upload")
//                        .file(filePdf)
//                        .file(jsonData)
//                )
//                .andReturn();
        ResultMatcher ok = MockMvcResultMatchers.status().isOk();

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.multipart("/v1/upload")
                .file(filePdf)
                .file(jsonData);
        this.mock.perform(builder).andExpect(ok);
    }
}
